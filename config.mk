# tabbed version
VERSION = 0.7

# paths
PREFIX = $(HOME)/opt/tabbed-${VERSION}
MANPREFIX = ${PREFIX}/share/man

X11INC = /usr/include/
X11LIB = /usr/lib

# freetype
FREETYPEINC = /usr/include/freetype2
FREETYPELIBS = -lfontconfig -lXft
# OpenBSD (uncomment)
#FREETYPEINC = ${X11INC}/freetype2

# includes and libs
INCS = -I. -I/usr/include -I$(X11INC) -I${FREETYPEINC}
LIBS = -L/usr/lib -lc -L${X11LIB} -lX11 ${FREETYPELIBS}

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE
CFLAGS = -std=c99 -pedantic -Wall -Os ${INCS} ${CPPFLAGS}
LDFLAGS = -s ${LIBS}

# compiler and linker
CC = cc
